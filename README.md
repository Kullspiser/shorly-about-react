# Shortly about React

Small presentation about basic features in React library.

## How to run it?

* Install [node.js](https://nodejs.org/en/)
* Clone this repo and run into root directory ```npm install && npm run start```
* Open in your browser [http://localhost:8000](http://localhost:8000)
* Enjoy =)